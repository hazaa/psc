"""
DATA:
    https://www.gov.uk/cma-cases  (concurrence)
        https://twitter.com/CMAgovUK
        https://competitionandmarkets.blog.gov.uk
        company number is in the full text report: https://assets.publishing.service.gov.uk/media/592ea50440f0b63e0b00013e/non-conf-decision-drawer_fronts.pdf

    https://www.gov.uk/cma-cases?parent=&keywords=&case_state%5B%5D=closed&closed_date%5Bfrom%5D=&closed_date%5Bto%5D=
    https://github.com/alphagov/finder-frontend
    https://docs.publishing.service.gov.uk/manual/incorrect-content-in-search-or-navigation.html
    https://content-api.publishing.service.gov.uk/#what-you-can-do-with-this-api
    curl https://www.gov.uk/api/content/cma-cases > cma-cases.txt

    BAD: curl https://www.gov.uk/cma-cases.json > cma-cases.json

    json viewer: https://docs.publishing.service.gov.uk/manual/incorrect-content-in-search-or-navigation.html
    https://medium.com/@nkhilv/how-to-use-the-javascript-fetch-api-to-get-uk-bank-holidays-step-by-step-dbb4357236ff

    !!FROM SEARCH API DIRECTLY
    curl https://www.gov.uk/api/search.json?count=100&filter_organisations=competition-and-markets-authority&filter_format=cma_case > cma-cases.json

SCRAPE/CRAWL
    automatic reports download: https://jamesthomas.uk/2021/05/03/downloading-files-from-the-cma-website-in-an-automated-way-using-r/
    scrapy:  https://blog.scrapinghub.com/
    dale_data... chap 6

    example: https://pastebin.com/yHsNgezk
            https://github.com/alphagov/govuk-lda-tagger/blob/master/govuk_spider.py

    https://github.com/alphagov/govuk_crawler_worker

PDF MINING
    https://github.com/drelhaj/CFIE-FRSE
    https://grobid.readthedocs.io/en
    https://textract.readthedocs.io/en
"""

import urllib.request
import json 
import joblib
from sklearn.feature_extraction.text import TfidfVectorizer,HashingVectorizer
import scipy.sparse as sp

from supervised import supervised_match

from psc import *
from cos_transform import *

import csv

from cfg import *


LIVE_COMPANIES_PATH = '{}/BasicCompanyDataAsOneFile-2019-09-01_cut.csv'.format(ROOT_DIR)


def match_query_live(): 
    """ 
    Read PSC company name (csv).
    Look for candidates in CMA cases using HashingVectorizer and sparse_dot_topn.
    Hand-labelled the best candidate CMA cases.
    Save to csv.
    """
    nmax_query=1000
    nmax_match=10
    #
    df_live = pd.read_csv(LIVE_COMPANIES_PATH, low_memory=False)
    df_live = clean_live_companies(df_live)
    #
    l =searchapi_parse_cos(nmax_query=nmax_query)
    #match_df = match_live_company(df_live["company_name"],l, ntop=5,
    #                            lower_bound=0.1)
    hv = HashingVectorizer()
    t = hv.transform(df_live["company_name"])
    t_new = hv.transform(l)
    # test cossim
    ntop=5
    lower_bound=0.1
    match_sparse = awesome_cossim_top(t_new, t.transpose(), ntop, lower_bound)
    # semi-supervised labelling from the short list obtained above.
    # https://docs.python.org/3/library/csv.html#csv.writer
    csvfile= open('data/cma_supervised_label.csv', 'a', newline='') 
    writer = csv.writer(csvfile, delimiter=',', dialect='unix')
    istart=231
    iend = 500#len(l)
    # match
    for i in range(istart,iend):
        target_name = l[i] #name_vector1[i]
        dum = match_sparse.getrow(i)
        index = dum.indices
        candidate_names = df_live["company_name"][index]
        #           
        title = "{}/{}".format(i,nmax_match)                 
        idx_choice = supervised_match(target_name,candidate_names,title)
        if not idx_choice is None:
            # append to file: target_name, candidate_names.iloc[idx_choice], index[idx_choice], df_live['company_number'][index[idx_choice]]
            row = [target_name, candidate_names.iloc[idx_choice], str(index[idx_choice]), str(df_live['company_number'][index[idx_choice]] ) ]
            writer.writerow(row) 
    csvfile.close()



def searchapi_parse_cos(nmax_query=1000):  
    """
    query the CMA search api, parse, transform to list.
    """ 
    # this is the main query
    query = 'https://www.gov.uk/api/search.json?count={}&filter_organisations=competition-and-markets-authority&filter_format=cma_case'.format(nmax_query)
       
    def merger_parser(x):
        """
        remove patterns: https://docs.python.org/3/library/re.html#re.Match.end
        """
        # find and remove 
        l_remove = ["The CMA","is investigating", "has investigated", "investigated",
                    "cleared", "the completed acquisition",
                    "the anticipated acquisition", "the anticipated merger",
                    ]
        for l in l_remove:
            m = re.search(l, x)
            if m is not None:
                x = x[:m.start()] + x[m.end():]
        # find pattern "by A... of A..." https://docs.python.org/3/library/re.html#re.Match.groupdict
        #m = re.match(r"by (?P<name_1>\w+) of (?P<name_2>\w+)", x)
        pattern = "\sby\s.+\sof\s"
        #pattern = "\bby\s+"
        #pattern = "\bby\b" #FAILS
        m = re.search(pattern, x)
        if m : return x[m.start()+3:m.end()-3]
        #else: return x
        # find pattern "between ... and ..."        
        
    # read cma cases 
    #https://docs.python.org/3/howto/urllib2.html#urllib-howto
    with urllib.request.urlopen(query) as response:
        html = response.read().decode()
        json_data = json.loads(html)
    results = json_data['results']
    # filter case=merger  
    merger_it = filter( lambda x: re.search("merger",x['_id']) is not None , results )
    l = []
    for d in merger_it:
        s= merger_parser(d['description'])
        if not s is None: l.append(str(s))
        print(s)
        #print(d['_id'])
        #print(d['description'])
    return l    
            
    
def test_hash_vectorizer_cos(nmax=10000):
    """
    Test HashingVectorizer
    Train and test samples are taken from the same database (=df_live, PSC data)
    """
    # read live companies:
    df_live = pd.read_csv(LIVE_COMPANIES_PATH, low_memory=False)
    df_live = clean_live_companies(df_live)
    #df = df_live["company_name"]
    df = df_live.iloc[0:nmax]["company_name"]
    df_new = df_live.iloc[0:100]["company_name"]
    # vectorize
    hv = HashingVectorizer()
    t = hv.transform(df)
    t_new = hv.transform(l) #hv.transform(df_new) FAILS??
    # test cossim
    ntop=10; lower_bound=0.1
    matches = awesome_cossim_top(t_new, t.transpose(), ntop, lower_bound)
    # compute matches
    matches_df = get_matches_df3(matches, df_new,df, top=1000)
    #matches_df[matches_df['similarity']>0.6]
    matches_df.sort_values(['similarity'], ascending=False).head(20)


################################################################
# DEPRECATED: TfidfVectorizer not adapted to large database


def train_save_vectorizer_live_company_names():
    # read live companies:
    df_live = pd.read_csv(LIVE_COMPANIES_PATH, low_memory=False)
    df_live = clean_live_companies(df_live)
    df = df_live["company_name"]
    # fit
    path_idf= '{}/tf_idf_live.npz'.format(ROOT_DIR)
    path_vocab= '{}/tf_vocab_live.joblib'.format(ROOT_DIR)
    # 
    v = fit_save_vectorizer(df,path_idf,path_vocab)


def test_fit_load_save_vectorizer_cos(nmax=10000):
    """
    problem: 
    """
    # read live companies:
    df_live = pd.read_csv(LIVE_COMPANIES_PATH, low_memory=False)
    df_live = clean_live_companies(df_live)
    #df = df_live["company_name"]
    df = df_live.iloc[0:nmax]["company_name"]
    # fit
    path_idf= '{}/tf_idf_live_test.npz'.format(ROOT_DIR)
    path_vocab= '{}/tf_vocab_live_test.joblib'.format(ROOT_DIR)
    v = fit_save_vectorizer(df,path_idf,path_vocab)
    # load
    vectorizer = load_vectorizer(path_idf,path_vocab)
    # test vectorizer
    tf_idf = vectorizer.transform(df)
    df_new = df_live.iloc[0:100]["company_name"]
    tf_idf_new = vectorizer.transform(df_new)
    # test cossim
    ntop=10; lower_bound=0.1
    matches = awesome_cossim_top(tf_idf_new, tf_idf.transpose(), ntop, lower_bound)


def fit_save_vectorizer(df,path_idf,path_vocab):
    """
    #http://thiagomarzagao.com/2015/12/08/saving-TfidfVectorizer-without-pickles/
    #https://blog.scrapinghub.com/2014/03/26/optimizing-memory-usage-of-scikit-learn-models-using-succinct-tries
    
    # !!!!!!!!!!!!!!!!!! hashing trick:
    #   https://scikit-learn.org/dev/modules/feature_extraction.html#vectorizing-a-large-text-corpus-with-the-hashing-trick
    """
    # train the vectorizer
    vectorizer = TfidfVectorizer(min_df=1, analyzer=ngrams)
    tf_idf = vectorizer.fit_transform(df)	
    # save
    #scipy.sparse.save_npz(path_idf, vectorizer.idf_)
    np.savez(path_idf, idf_=vectorizer.idf_)
    #json.dump(vectorizer.vocabulary_, open(path_vocab, mode = 'wb'))
    with open(path_vocab, 'wb') as fo:  
        joblib.dump(vectorizer.vocabulary_, fo)



def load_vectorizer(path_idf,path_vocab):
    """
    """
    #load
    #idfs = scipy.sparse.load_npz(path_idf)
    nzfile= np.load(path_idf)
    idfs=nzfile['idf_']
    class MyVectorizer(TfidfVectorizer):
        # plug our pre-computed IDFs
        TfidfVectorizer.idf_ = idfs
    #npzfile = np.load(path_idf)
    #idfs = npzfile['idf']
    #vocabulary = json.load(open(path_vocab, mode = 'rb'))
    with open(path_vocab, 'rb') as fo:    
        vocabulary=joblib.load(fo)
    # instantiate vectorizer
    vectorizer = MyVectorizer(min_df=1, analyzer=ngrams)
    # plug in
    vectorizer.vocabulary_ = vocabulary
    vectorizer._tfidf._idf_diag = sp.spdiags(idfs,
                                         diags = 0,
                                         m = len(idfs),
                                         n = len(idfs))
    return vectorizer

