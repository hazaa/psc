#!/usr/bin/env bash

#https://github.com/nkoub/the-art-of-command-line

# psc file: keep only lines containing "corporate-entity"
# see https://www.datascienceatthecommandline.com/chapter-5-scrubbing-data.html#filtering-lines
#for filename in psc-snapshot-2019-10-22*of15.txt
cd /home/aurelien/local/data/psc

for filename in psc-snapshot-2019-12-02*of15.txt
do
echo "keep only corporate-entity in: $filename, put into: ${filename%.txt}_corporateonly.txt"
grep corporate-entity $filename > "${filename%.txt}_corporateonly.txt"
done
