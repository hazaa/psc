# -*- coding: utf-8 -*-
"""

FUNCTIONS COPIED FROM 
    https://github.com/Global-Witness/
    https://github.com/Global-Witness/uk-beneficial-ownership-analysis-2019-public

"""

def standardise_columns(columns):
    output = [x.replace(' ', '_') for x in columns]
    output = [x.lower() for x in output]
    output = [x.replace('.', '_') for x in output]
    print('Standardized df columns...')
    return output
    
def clean_live_companies(df):
    output = df.copy()
    output.columns = [x.strip() for x in output.columns]
    output.columns = standardise_columns(output)
    output.rename(
        columns={
            'companynumber': 'company_number',
            'companyname': 'company_name'
        },
        inplace=True)
    print('Cleaned live companies...')
    return output

def read_psc_json(path):
    temp_df = pd.read_json(path, lines=True)#,chunksize=1000)
    output_df = pd.concat(
        [temp_df['company_number'],
         json_normalize(temp_df['data'])], axis=1)
    print('Read JSON file...')
    return output_df

"""
https://pandas.pydata.org/pandas-docs/stable/user_guide/io.html#normalization
https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.io.json.json_normalize.html#pandas.io.json.json_normalize

'company_number' 'name' 'natures_of_control' 'identification.registration_number'

l=[json.loads(line) for line in open(PSC_FILE_PATH)]
df=json_normalize(l)
df=df.explode('natures_of_control') #new version 0.25

temp_df = pd.read_json(path, lines=True)#,chunksize=1000)
json_normalize(temp_df['data'], ['natures_of_control','name',
				['identification','registration_number']])
df_psc = json_normalize(temp_df['data'], 'natures_of_control',['name', 
    ['identification','registration_number']], errors='ignore') 
df_psc = json_normalize(temp_df, ['company_number','name'],['natures_of_control', 
    'identification','registration_number']], errors='ignore')		 
    
"""


def company_code_creator(x):
    if x[:2].isdigit():
        return 'EAW'
    else:
        return x[:2]

def remove_no_record_rows(df):
    # remove last line of DataFrame which is not a record
    output_df = df.iloc[:-1].copy()
    # remove summary totals
    output_df = output_df[
        output_df.kind != 'totals#persons-of-significant-control-snapshot']
    print('Removed extra rows...')
    return output_df

def create_join_id(x, first_name_col, surname_col, month_year_birth_col):
    if not x.isnull().values.any():
        first_name = x[first_name_col].split(' ')[0]
        month_year = x[month_year_birth_col].strftime('%Y-%m')
        join_id = first_name + '-' + x[surname_col] + '_' + month_year
        join_id = join_id.strip('-_ ')
        join_id = join_id.upper()
        join_id = join_id.replace(' ', '')
        return join_id
    else:
        return np.nan


def create_additional_columns_all_records(df, live_company_map):
    #url_company_codes, url_company_codes_s = create_url_company_codes(
    #    URL_COMPANY_CODES_PATH)
    temp_df = df.copy()
    temp_df['month_year_birth'] = temp_df['date_of_birth_year'].dropna(
    ).astype(str).str.replace(
        r'\.0', '') + '-' + temp_df['date_of_birth_month'].dropna().astype(
            str).str.replace(r'\.0', '')
    temp_df['month_year_birth'] = pd.to_datetime(
        temp_df['month_year_birth'], format='%Y-%m', errors='coerce')
    temp_df['join_id'] = temp_df[[
        'name_elements_forename', 'name_elements_surname', 'month_year_birth'
    ]].apply(
        create_join_id,
        first_name_col='name_elements_forename',
        surname_col='name_elements_surname',
        month_year_birth_col='month_year_birth', axis=1)
    temp_df['type_codes'] = temp_df.company_number.apply(company_code_creator)
    temp_df['company_name'] = temp_df.company_number.map(
        live_company_map['company_name'])
    temp_df['company_first_and_postcode'] = temp_df.company_number.map(
        live_company_map['first_and_postcode'])
    print('Created additional columns on all records df...')
    return temp_df

