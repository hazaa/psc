# -*- coding: utf-8 -*-
"""

TODO:
*  comparer les conditions nécessaires pour les algos ci-dessous 
	avec l'info dispo en pratique. (lifi,psc)
   

MATRIX COMPLETION
* https://github.com/iskandr/fancyimpute
* REFS: montanari 09 https://doi.org/10.1109/isit.2009.5205567

NETWORK RECONSTRUCTION
* Reconstruction of multiplex networks with correlated layers
  https://arxiv.org/abs/1709.03918 

GRAPH , "LINK PREDICTION"
* https://arxiv.org/abs/1606.06812
* Pech "Link prediction via linear optimization" https://arxiv.org/abs/1804.00124
* parisi garla squart https://arxiv.org/abs/1802.02064

* https://github.com/search?p=2&q=link+prediction&type=Repositories
* https://github.com/lucashu1/link-prediction
* https://github.com/rafguns/linkpred

* check alves !!
https://github.com/lgaalves/school_crime_and_corruption_analysis/blob/master/day-6-complex-network-analysis-of-corruption/03%20-%20Corruption%20Network.ipynb
"""



def test_psc():
    def_psc = read_psc(PSC_FILE_PATH)
