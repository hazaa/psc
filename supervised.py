# -*- coding: utf-8 -*-

""" 
supervised annotation using the terminal

dialogs:
https://python-prompt-toolkit.readthedocs.io/en/stable/pages/dialogs.html#yes-no-confirmation-dialog
https://python-prompt-toolkit.readthedocs.io/en/stable/pages/dialogs.html#button-dialog

completion:
https://python-prompt-toolkit.readthedocs.io/en/stable/pages/asking_for_input.html#autocompletion

progess bar
https://python-prompt-toolkit.readthedocs.io/en/stable/pages/progress_bars.html
"""
    
from cma_cases import searchapi_parse_cos
from prompt_toolkit.shortcuts import radiolist_dialog


def supervised_match(name_target,name_candidates,title):
    """ 
    """
    val = [(j,cand) for j,cand in enumerate(name_candidates)   ]
    result = radiolist_dialog(
        title=title,
        text=name_target,
        values=val)
    
    return result

# ------------------------------------------------------
# TESTS

def test_button():
    A = [['affffffffggggggg','brrrrrrrrrrrrrr','cfvvvvvvvvvvvvvvvvv', 'llllllllllllllllllkkkk' , 'kkkkkkkkkkkkkyyyyyyyyyyyytu'],
          ['hffffffffggggggg','arrrrrrrrrrrrrr','yfvvvvvvvvvvvvvvvvv', 'ylllllllllllllllllkkkk' , 'okkkkkkkkkkkkyyyyyyyyyyyytu'] ]   
    for l in A:
        but = [ (ll,i) for i,ll in enumerate(l)  ]
        result = button_dialog(
            title='Button dialog example',
            text='Do you want to confirm?',
            buttons = but )  
        print(result) 

def test_radio_basic():
    """ basic test"""
    result = radiolist_dialog(
        title="RadioList dialog",
        text="Which breakfast would you like ?",
        values=[
            ("breakfast1", "Eggs and beacon"),
            ("breakfast2", "French breakfast"),
            ("breakfast3", "Equestrian breakfast")
        ]).run()

def test_radio():
    """ """
    l_target   =['fsdsfd','dfsdfsduy']
    n = len(l_target)
    l_candidate=[['fsdsfd','pojil'],['dfsdfsduy','ff', 'dfz87a']]
    for i,(str_target,str_candidate) in enumerate(zip(l_target,l_candidate)):
        val = [(j,cand) for j,cand in enumerate(str_candidate)   ]
        result = radiolist_dialog(
            title="RadioList dialog {}/{}".format(i,n),
            text=str_target,
            values=val)
        print(result)    
        
    
