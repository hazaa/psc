# PSC

Build and analyze graphs from PSC data.

*  psc.py: build multiplex networks from PSC data. Compute basic network metrics.
*  notebooks: 
   * psc_mplex.ipynb: plot multiplex

![alt text](fig/psc_subnet.png "PSC subnetwork")

# PSC/CMA

Match PSC and CMA firm data.

*  cma_cases.py: 
   * searchapi_parse_cos(): query the search api, then parse, then transform to list.   
   * match_query_live(): hand labelling of CMA cases using PSC company names. Save to csv.
*  supervised.py: supervised annotation using the terminal.   
*  notebooks: 
   *   supervised.ipynb: match PSC and labelled CMA firm data.


#  Data

* Get PSC data:
   * http://download.companieshouse.gov.uk/en_pscdata.html
   * http://download.companieshouse.gov.uk/en_output.html

Then process using bash scripts *.sh in the directory.



# Related links

* https://github.com/Global-Witness/
