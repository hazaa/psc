# -*- coding: utf-8 -*-

"""
FUNCTIONS copied from https://bergvca.github.io/2017/10/14/super-fast-string-matching.html

https://github.com/ing-bank/sparse_dot_topn
"""

import numpy as np
import pandas as pd
import sparse_dot_topn.sparse_dot_topn as ct
from sklearn.feature_extraction.text import TfidfVectorizer
import scipy.sparse
from scipy.sparse import csr_matrix

import re


def ngrams(string, n=3):
    string = re.sub(r'[,-./]|\sBD',r'', string)
    ngrams = zip(*[string[i:] for i in range(n)])
    return [''.join(ngram) for ngram in ngrams]

def awesome_cossim_top(A, B, ntop, lower_bound=0):
    # force A and B as a CSR matrix.
    # If they have already been CSR, there is no overhead
    A = A.tocsr()
    B = B.tocsr()
    M, _ = A.shape
    _, N = B.shape
 
    idx_dtype = np.int32
 
    nnz_max = M*ntop
 
    indptr = np.zeros(M+1, dtype=idx_dtype)
    indices = np.zeros(nnz_max, dtype=idx_dtype)
    data = np.zeros(nnz_max, dtype=A.dtype)

    ct.sparse_dot_topn(
        M, N, np.asarray(A.indptr, dtype=idx_dtype),
        np.asarray(A.indices, dtype=idx_dtype),
        A.data,
        np.asarray(B.indptr, dtype=idx_dtype),
        np.asarray(B.indices, dtype=idx_dtype),
        B.data,
        ntop,
        lower_bound,
        indptr, indices, data)

    return csr_matrix((data,indices,indptr),shape=(M,N))

def get_matches_df(sparse_matrix, name_vector, top=100):
    non_zeros = sparse_matrix.nonzero()
    
    sparserows = non_zeros[0]
    sparsecols = non_zeros[1]
    
    if top:
        nr_matches = top
    else:
        nr_matches = sparsecols.size
    
    left_side = np.empty([nr_matches], dtype=object)
    right_side = np.empty([nr_matches], dtype=object)
    similarity = np.zeros(nr_matches)
    
    for index in range(0, nr_matches):
        left_side[index] = name_vector[sparserows[index]]
        right_side[index] = name_vector[sparsecols[index]]
        similarity[index] = sparse_matrix.data[index]
    
    return pd.DataFrame({'left_side': left_side,
                          'right_side': right_side,
                           'similarity': similarity})


def vectorize(df_live,df_new,path_tf_idf_live,path_tf_idf_new):	
	"""
	see:   
    https://github.com/Cheukting/fuzzy-match-company-name/blob/master/Fuzzy-match-company-names.ipynb
    https://medium.com/bcggamma/an-ensemble-approach-to-large-scale-fuzzy-name-matching-b3e3fa124e3c
    https://bergvca.github.io/2017/10/14/super-fast-string-matching.html
    ING: https://medium.com/wbaa/https-medium-com-ingwbaa-boosting-selection-of-the-most-similar-entities-in-large-scale-datasets-450b3242e618
    
    https://scikit-learn.org/stable/modules/feature_extraction.html
    
    save:
    https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.save_npz.html#scipy.sparse.save_npz
	"""
	# fit the vectorizer, transform then save
	vectorizer = TfidfVectorizer(min_df=1, analyzer=ngrams)
	
	tf_idf_live = vectorizer.fit_transform(df_live["company_name"])	
	scipy.sparse.save_npz(path_tf_idf_live, tf_idf_live)
	# keep the same dictionary
	# DO NOT USE: tf_idf_psc = vectorizer.fit_transform(df_psc["name"])
	# instead, use vectorizer.transform :
	tf_idf_new = vectorizer.transform(df_new["name"])
	scipy.sparse.save_npz(path_tf_idf_new, tf_idf_new)	
	


def compute_cos(path_tf_idf_psc,path_tf_idf_live,path_cossim,
				ntop=10, lower_bound=0):
	"""
	compute cosine transform 
	"""
	# load
	tf_idf_psc = scipy.sparse.load_npz(path_tf_idf_psc)
	tf_idf_live = scipy.sparse.load_npz(path_tf_idf_live)
	# transform
	t1 = time.time()
	matches = awesome_cossim_top(tf_idf_psc, tf_idf_live.transpose(), ntop, lower_bound)
	t = time.time()-t1
	print("SELFTIMED:", t)
	print("matches.nnz={}  matches.shape[0]=".format(matches.nnz,matches.shape[0]))
	# save
	scipy.sparse.save_npz(path_cossim,matches)



########################################################################
# MODIFICATIONS



def get_matches_df2(sparse_matrix, name_vector1,name_vector2, top=100):
    non_zeros = sparse_matrix.nonzero()
    
    sparserows = non_zeros[0]
    sparsecols = non_zeros[1]
    
    if top:
        nr_matches = top
    else:
        nr_matches = sparsecols.size
    
    left_side = np.empty([nr_matches], dtype=object)
    right_side = np.empty([nr_matches], dtype=object)
    similarity = np.zeros(nr_matches)
    
    for index in range(0, nr_matches):
        left_side[index] = name_vector1[sparserows[index]]
        right_side[index] = name_vector2[sparsecols[index]]
        similarity[index] = sparse_matrix.data[index]
    
    return pd.DataFrame({'left_side': left_side,
                          'right_side': right_side,
                           'similarity': similarity})
def get_matches_df3(sparse_matrix, name_vector1,name_vector2, top=30):                          
	"""
	"""
	nr,nc = sparse_matrix.shape
	
	if top:
		nr_matches = top
	else:
		nr_matches = sparsecols.size
	
	left_side = np.empty([nr_matches], dtype=object)
	right_side = np.empty([nr_matches], dtype=object)
	similarity = np.zeros(nr_matches)
	index = np.zeros(nr_matches)
	for i in range(0, nr_matches):
		left_side[i] = name_vector1[i]
		dum = sparse_matrix.getrow(i)
		ind = dum.argmax()
		index[i]=ind
		right_side[i] = name_vector2[ind]
		x = dum[0,ind]
		similarity[i] = x
	return pd.DataFrame({'left_side': left_side,
                          'right_side': right_side,
                           'similarity': similarity,
                           'index':index})    
