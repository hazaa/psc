# -*- coding: utf-8 -*-
"""
Read PSC data. Match company names. 
Build networks, monoplex/multiplex. Plot.
 

Data:
    http://download.companieshouse.gov.uk/en_pscdata.html
    http://download.companieshouse.gov.uk/en_output.html

Company register: 
    https://beta.companieshouse.gov.uk/

Data description:
    https://developer.companieshouse.gov.uk/api/docs/company/company_number/persons-with-significant-control/corporateEntity-resource.html

Dependences (pip):
	sparse_dot_topn: https://bergvca.github.io/2017/10/14/super-fast-string-matching.html
    (pythran, sklearn)
    
Defs:
    pandas.pydata.org
"""
import numpy as np
import pandas as pd
from pandas.io.json import json_normalize
import matplotlib.pyplot as plt

import re
from sklearn.feature_extraction.text import TfidfVectorizer
import scipy.sparse
from scipy.sparse import csr_matrix

from global_witness import *
from cos_transform import *
#https://github.com/ing-bank/sparse_dot_topn
import sparse_dot_topn.sparse_dot_topn as ct
import time
	
from pymnet import *
import networkx as nx
from mplex.mplexio import get_DiG_from_MultiDiG
from mplex.comtrade_multiplex import analyze_single_layer_nx	
	
from cfg import *    
	
#PSC_FILE_PATH = '{}/persons-with-significant-control-snapshot-2019-03-05.txt'.format(ROOT_DIR)
#PSC_FILE_PATH = '{}/psc/psc-snapshot-2019-09-11_1of15.txt'.format(ROOT_DIR)        
PSC_FILE_PATH = '{}/psc-snapshot-2019-10-22_1of15_corporateonly.txt'.format(ROOT_DIR)
# head -1000 file.txt > out.txt
#PSC_FILE_PATH = '{}/psc/psc-snapshot-2019-09-11_test.txt'.format(ROOT_DIR)    
#LIVE_COMPANIES_PATH = '{}/psc/BasicCompanyDataAsOneFile-2019-09-01.csv'.format(ROOT_DIR)
LIVE_COMPANIES_PATH = '{}/BasicCompanyDataAsOneFile-2019-09-01_cut.csv'.format(ROOT_DIR)


def get_psc_path_list(date='2019-12-02',start=1,stop=15):
	return ['{}/psc-snapshot-{}_{}of{}_corporateonly.txt'.format(ROOT_DIR,date,i,stop) for i in range(start,stop+1)]

	
def main_global_witness():
	"""
	Global-Witness
	"""
	nrows = 10000# None
	# PROCESS LIVE COMPANIES
	#from load_live_companies(path)
	df = pd.read_csv(LIVE_COMPANIES_PATH, low_memory=False, nrows=nrows)
	live_companies = clean_live_companies(df)
	live_companies_list = live_companies.company_number.unique().tolist()
	live_company_map = live_companies[[
        'company_number', 'company_name'#, 'first_and_postcode'
    ]].set_index('company_number')
	# PROCESS PSC 
	psc_json_path = PSC_FILE_PATH
	all_records = read_psc_json(open(psc_json_path))
	all_records = remove_no_record_rows(all_records)
	all_records.columns = standardise_columns(all_records.columns)
	all_records = create_additional_columns_all_records(
        all_records, live_company_map)
    #psc_records = create_records_psc_df(all_records)

def clean_live_companies(df):
    output = df.copy()
    output.columns = [x.strip() for x in output.columns]
    output.columns = standardise_columns(output)
    output.rename(
        columns={
            'companynumber': 'company_number',
            'companyname': 'company_name'
        },
        inplace=True)
    print('Cleaned live companies...')
    return output

def read_live_psc_name(LIVE_COMPANIES_PATH,PSC_FILE_PATH):
	"""keep only the name of the psc that has control over entity
	"""
	df_live = pd.read_csv(LIVE_COMPANIES_PATH, low_memory=False)
	df_live = clean_live_companies(df_live)
	# read psc: json->pandas
	psc_json_path = PSC_FILE_PATH
	df_psc = pd.read_json(psc_json_path, lines=True)#,chunksize=1000)
	df_psc = pd.concat(
		[ #df_psc['company_number'],
		 json_normalize(df_psc['data'])], axis=1)
	# keep only selected columns	 
	df_psc= pd.concat(	 
		 [#df_psc['company_number'],
		 df_psc['name']
		 #,df_psc['natures_of_control' ]
		 #,temp_df['kind']
		 ],
		 axis=1)	
	# move psc name to upper case (like live companies)	 
	df_psc['name'] = df_psc['name'].str.upper() 
	return df_live,	df_psc 

def read_psc(PSC_FILE_PATH):
	# read psc: json->pandas
	psc_json_path = PSC_FILE_PATH
	df_psc = pd.read_json(psc_json_path, lines=True)#,chunksize=1000)
	df_psc = pd.concat(
		[df_psc['company_number'],
		 json_normalize(df_psc['data'])], axis=1)
	# keep only selected columns	 
	df_psc= pd.concat(	 
		 [df_psc['company_number'],
		 df_psc['name'],df_psc['natures_of_control' ],
		 df_psc['identification.registration_number'],
		 #,temp_df['kind']
		 ],
		 axis=1)

	return df_psc	 	

def read_live_psc(LIVE_COMPANIES_PATH,PSC_FILE_PATH):
	"""
	"""
	# read live companies:
	df_live = pd.read_csv(LIVE_COMPANIES_PATH, low_memory=False)
	df_live = clean_live_companies(df_live)
	# read psc
	def_psc = read_psc(PSC_FILE_PATH)
	return df_live,	df_psc 
	
####################################################################
# NAME MATCHING, FOR TESTING PURPOSE ONLY

def test_match():
	"""
	TODO:
	1- read psc
	2- find psc with no registration
	3- match the latter using (live companies + registered psc)
	"""
	path_tf_idf_live = '{}/tf_idf_live.npz'.format(ROOT_DIR)	
	path_tf_idf_psc = '{}/tf_idf_psc.npz'.format(ROOT_DIR)	
	path_cossim = '{}/cos_live_psc.npz'.format(ROOT_DIR)	
	# load
	df_live,df_psc = read_live_psc_name(LIVE_COMPANIES_PATH,PSC_FILE_PATH)	
	# vectorize, compute cos transform, save results
	vectorize(df_live,df_psc,path_tf_idf_live,path_tf_idf_psc)
	compute_cos(path_tf_idf_psc,path_tf_idf_live,path_cossim,
				ntop=10, lower_bound=0.1)
	# load cos matrix
	matches = scipy.sparse.load_npz(path_cossim)		
	# compute matches
	matches_df = get_matches_df3(matches, df_psc['name'],df_live["company_name"], top=1000)
	#matches_df[matches_df['similarity']>0.6]
	matches_df.sort_values(['similarity'], ascending=False).head(20)

def test_company_name_matching():
	"""
	try to find the company id
	for the ~15% of companies that don't have an identifier.
	"""
	path= PSC_FILE_PATH
	tf_idf_vec = True
	df_psc = read_psc(path)
	# ratio of registered psc
	ratio = 100*np.sum(1*pd.notna(df_psc['identification.registration_number']))/float(df_psc.shape[0])
	print('ratio of registered psc={} %'.format(ratio))
	# remove unregistered
	#df_psc = df_psc[ pd.notna(df_psc['identification.registration_number'])]
	# upper-case
	df_psc['identification.registration_number'] = df_psc['identification.registration_number'].str.upper() 
	# look at self-mismatches
	if tf_idf_vec:
		vectorizer = TfidfVectorizer(min_df=1, analyzer=ngrams)
		tf_idf_psc_registered = vectorizer.fit_transform(df_psc['identification.registration_number'])	
		matches = awesome_cossim_top(tf_idf_psc_registered, tf_idf_psc_registered.transpose(),
									10, 0.8)
		match_df = get_matches_df(matches, df_psc['identification.registration_number'].tolist())
		match_df[match_df['similarity']<1] # WHAT TO DO WITH IT ??

####################################################################
# MPLEX

def snapshot_psc_to_mplex(date_='2019-12-02',backend = "pymnet", stop=15,nmax=1):
	"""
	from psc only
	"""
	mplex = None
	#outputfiles = '{}/mplex_psc_{}.edg'.format(ROOT_DIR,date_)
	for i,path in enumerate(get_psc_path_list(date=date_,start=1,stop=stop)):
	#for path in [PSC_FILE_PATH]:
		print(path)
		read_ok = False
		try:
			df_psc = read_psc(path)
			read_ok = True
		except:
			print('read failed')
			pass
		if read_ok:			
			# ratio of registered psc
			ratio = 100*np.sum(1*pd.notna(df_psc['identification.registration_number']))/float(df_psc.shape[0])
			print('ratio of registered psc={} %'.format(ratio))
			# filter NaN (unregistered)
			df_psc = df_psc[ pd.notna(df_psc['identification.registration_number'])]
			# filter incorrect registration_number
			#     id contains spacing
			idx=df_psc['identification.registration_number'].str.contains(' ', regex=False)
			df_psc = df_psc[idx.ne(True)]
			#     id is not 8 chars long : if 6 or 7 integer, pad with 0 
			#     company_name is incorrect
			# upper-case
			df_psc['identification.registration_number'] = df_psc['identification.registration_number'].str.upper() 
			df_psc['company_number'] = df_psc['company_number'].str.upper() 
			# check/clean the company registration_number
			# ??
			# add nodes and edges to multiplex
			if backend=='pymnet':
				mplex = multiplex_from_snapshot_psc(df_psc,mplex=mplex)		
			elif backend=='nx':	
				mplex = mDiG_from_snapshot_psc(df_psc,G=mplex)
			else: raise ValueError('unknown backend')	
		if i+1>=nmax:break            			
	return mplex

def save_psc_to_edge_file_pymnet(mplex):
	"""
	"""
	# save
	# gml ??
	pymnet.netio.write_edge_files(mplex,outputfiles,columnSeparator="\t",rowSeparator="\n",weights=True,masterFile=False,numericNodes=False)



def analyze_psc_nx():
	"""
	backend=nx (py3plex)
	
	path=PSC_FILE_PATH
	df_psc = read_psc(path)
	# ratio of registered psc
	ratio = 100*np.sum(1*pd.notna(df_psc['identification.registration_number']))/float(df_psc.shape[0])
	print('ratio of registered psc={} %'.format(ratio))
	# remove unregistered
	df_psc = df_psc[ pd.notna(df_psc['identification.registration_number'])]
	# upper-case
	df_psc['identification.registration_number'] = df_psc['identification.registration_number'].str.upper() 
	# get MultiDiGraph
	G = mDiG_from_snapshot_psc(df_psc,G=None)"""
	#convert
	G=snapshot_psc_to_mplex(backend = "nx")
 
	for layer in ['a']:
		G_DiG = get_DiG_from_MultiDiG(G,layer) 
		#analyse	
		analyze_single_layer_nx(G_DiG, plot=True)
		#
		net=G_DiG
		comp = sorted(nx.weakly_connected_components(G),key=len, reverse=True)
		len_weak_conn_comp = [len(c) for c in comp]
		d=G.out_degree() 
		plt.hist( list(dict(d).values() ),100 ); plt.show()

def test_analyse_edge_file_igraph():
	# load edge_file edga, edgb, ..> igraph list [g1,..]
	# 		
	# from comtrade_multiplex import read_edge_files_igraph
	pass

def analyse_psc_pymnet():
	# load edge_file
	# 		read_edge_files not appropriate
	# ----------
	# backend=pymnet
	# simple plots
	# one layer
	mplex = snapshot_psc_to_mplex(backend = "pymnet")
	nodes = mplex.slices[0]
	layers = mplex.slices[1]
	sub_net = subnet(mplex, nodes, 'a')
	#nnx=nxwrap.MonoplexGraphNetworkxView(sub_net);	nxwrap.draw(nnx) #FAILS!
	# select top node-layer (i,alpha) by degree 
	top = 10
	idx = np.argsort(list(d.values()))[0:top] 
	top_node_layer =  [list(d.keys())[i] for i in idx]
	# plot neighbors of a node	
	d = degs(mplex,'nodes')
	l_,n_ = 'b','01823605'
	nodes = [neigh for neigh in mplex.A[l_][n_]]
	nodes.append(n_)
	sub_net = subnet(mplex, nodes,layers)
	draw(sub_net,layout='random') 
	plt.show()
	webplot(sub_net,'out.html')	
	# n-th order neighbors ?
	# --------------
	#  plot interactive
	# => create ipynb with widgets first. Then serve:
	#https://github.com/oschuett/appmode
	#https://github.com/QuantStack/voila-gridstack/
	
	# -----------
	# TODO
	# basic statistics
	# degree distribution
	# knn in/out : interpret
	# plot largest connex components (undirected case)	
	# plot tubes, tendrils, etc... (directed case)	
	# distribution of diameter of connex components 
	# Reconstruct 1 layer from the others 
	# join with "Competition and Markets Authority cases" https://www.gov.uk/cma-cases
	# clustering mplex (louvain...), explain clusters

	
def multiplex_from_snapshot_psc(df_psc,mplex=None):	
	"""
	backend=pymnet , http://www.mkivela.com/pymnet/
	
	
	muliplex: one layer per 'natures_of_control' (own shares, voting rights...)
      layer 1: ownership-of-shares-{25-to-50, ...}-percent
      layer 2: voting-rights-{75-to-100}-percent
      layer 3: right-to-appoint-and-remove-directors
	"""
	if mplex is None:
		mplex = MultiplexNetwork(couplings="none",directed=True)
	for i in range(df_psc.shape[0]):
		# create nodes
		orig = df_psc.iloc[i]['identification.registration_number']
		dest = df_psc.iloc[i]['company_number']
		# discard self-links
		if orig==dest: break
		mplex.add_node(orig)
		mplex.add_node(dest)
		# add links 
		for n in df_psc.iloc[i]['natures_of_control' ]:
			#right-to-appoint-and-remove-*
			if n[0]=="r" and n[9]=="a":	
				mplex[orig,'a'][dest,'a']=1
			# voting rights 	
			elif n[0]=="v":
				if n[14:16]=="25":
					mplex[orig,'b'][dest,'b']=1
				elif n[14:16]=="50":	
					mplex[orig,'b'][dest,'b']=2
				elif n[14:16]=="75":		
					mplex[orig,'b'][dest,'b']=3
				else:
					print(n)
					raise ValueError('unknown voting-rights')	
			# ownership
			elif n[0]=="o":		
				if n[20:22]=="25":
					mplex[orig,'c'][dest,'c']=1
				elif n[20:22]=="50":	
					mplex[orig,'c'][dest,'c']=2
				elif n[20:22]=="75":		
					mplex[orig,'c'][dest,'c']=3
				else:
					print(n)
					raise ValueError('unknown ownership')	
			else:
				"""
				'ownership-of-shares-25-to-50-percent'
				'ownership-of-shares-25-to-50-percent-as-firm'
				'voting-rights-75-to-100-percent-limited-liability-partnership'
				'voting-rights-75-to-100-percent-as-trust-limited-liability-partnership'
				'voting-rights-75-to-100-percent-as-firm-limited-liability-partnership'
				'right-to-appoint-and-remove-directors'
				'right-to-appoint-and-remove-directors-as-firm'
				'right-to-appoint-and-remove-directors-as-trust'
				'right-to-appoint-and-remove-members-limited-liability-partnership'
				'right-to-appoint-and-remove-members-as-firm-limited-liability-partnership'
				'right-to-share-surplus-assets-75-to-100-percent-limited-liability-partnership'
				'right-to-share-surplus-assets-75-to-100-percent-as-trust-limited-liability-partnership'
				'significant-influence-or-control'
				'significant-influence-or-control-as-firm-limited-liability-partnership'
				'significant-influence-or-control-as-trust-limited-liability-partnership'
				"""
				pass#raise ValueError('unknown natures_of_control')	
			
	return mplex		
		
def mDiG_from_snapshot_psc(df_psc,G=None):	
	"""
	backend = py3plex , https://github.com/SkBlaz/Py3plex/
	
	muliplex: one layer per 'natures_of_control' (own shares, voting rights...)
      layer 1: ownership-of-shares-{25-to-50, ...}-percent
      layer 2: voting-rights-{75-to-100}-percent
      layer 3: right-to-appoint-and-remove-directors
      
      
     G.add_node((node_first,str(layer)))
     G.add_node((node_second,str(layer)))
     G.add_edge((node_first,str(layer)),(node_second,str(layer)),weight=float(weight))#,type="default")
  
	"""
	if G is None:
		G = nx.MultiDiGraph()
	for i in range(df_psc.shape[0]):
		# create nodes
		orig = df_psc.iloc[i]['identification.registration_number']
		dest = df_psc.iloc[i]['company_number']
		# discard self-links
		if orig==dest: break
		# add links 
		for n in df_psc.iloc[i]['natures_of_control' ]:
			#right-to-appoint-and-remove-*
			if n[0]=="r" and n[9]=="a":	
				G.add_node((orig,'a'))
				G.add_node((dest,'a'))
				G.add_edge((orig,'a'),(dest,'a'),weight=1.)
				#mplex.add_node(orig)
				#mplex.add_node(dest)				
				#mplex[orig,'a'][dest,'a']=1
			# voting rights 	
			elif n[0]=="v":
				G.add_node((orig,'b'))
				G.add_node((dest,'b'))
				if n[14:16]=="25":
					G.add_edge((orig,'b'),(dest,'b'),weight=1)
					#mplex[orig,'b'][dest,'b']=1
				elif n[14:16]=="50":
					G.add_edge((orig,'b'),(dest,'b'),weight=2)	
					#mplex[orig,'b'][dest,'b']=2
				elif n[14:16]=="75":		
					G.add_edge((orig,'b'),(dest,'b'),weight=3)
					#mplex[orig,'b'][dest,'b']=3
				else:
					print(n)
					raise ValueError('unknown voting-rights')	
			# ownership
			elif n[0]=="o":	
				G.add_node((orig,'c'))
				G.add_node((dest,'c'))	
				if n[20:22]=="25":
					G.add_edge((orig,'c'),(dest,'c'),weight=1)
					#mplex[orig,'c'][dest,'c']=1
				elif n[20:22]=="50":	
					G.add_edge((orig,'c'),(dest,'c'),weight=2)
					#mplex[orig,'c'][dest,'c']=2
				elif n[20:22]=="75":
					G.add_edge((orig,'c'),(dest,'c'),weight=3)		
					#mplex[orig,'c'][dest,'c']=3
				else:
					print(n)
					raise ValueError('unknown ownership')	
			else:
				pass#raise ValueError('unknown natures_of_control')	
			
	return G


def create_multiplex_from_timedep_psc(df_psc):	
	#   
	#   the same with layers = time
	pass


################################################################
	
def deprecated():	
	"""
	"""
		# live companies:
	df_live = pd.read_csv(LIVE_COMPANIES_PATH, low_memory=False)
	df_live = clean_live_companies(df_live)
	# psc: json->pandas
	psc_json_path = PSC_FILE_PATH
	df_psc = pd.read_json(psc_json_path, lines=True)#,chunksize=1000)
	df_psc = pd.concat(
		[df_psc['company_number'],
		 json_normalize(df_psc['data'])], axis=1)
	# keep only psc corresponding to corporate-entity 
	if False:
		# deprecated, see preprocess
		temp_df = temp_df[ temp_df['kind']== 'corporate-entity-person-with-significant-control']
	# keep only selected columns	 
	df_psc= pd.concat(	 
		 [df_psc['company_number'],
		 df_psc['name'],df_psc['natures_of_control' ]
		 #,temp_df['kind']
		 ],
		 axis=1)
	# TRY NAIVE JOIN 
	# https://pandas.pydata.org/pandas-docs/stable/user_guide/merging.html#database-style-dataframe-or-named-series-joining-merging
	if False:
			# RESULT: not a single match
			result = pd.merge(output_df, 
						live_companies[['company_name', 'company_number']].rename(columns={"company_name": "company_name_ctrl"}),
						how='left', left_on='name', right_on='company_name_ctrl')
			print("nb of matches=",np.sum(1*pd.notna(result['company_name_ctrl']))		)	
	# TRY STRING MATCHING	
	#https://stackoverflow.com/questions/13636848/is-it-possible-to-do-fuzzy-match-merge-with-python-pandas 
	if False: 
		# RESULT: much too long
		s = live_companies['company_name'].tolist()
		limit=2
		m = output_df["name"][1:3].apply(lambda x: process.extract(x, s, limit=limit))    
    # TRY 
    # https://github.com/Cheukting/fuzzy-match-company-name/blob/master/Fuzzy-match-company-names.ipynb
    # https://medium.com/bcggamma/an-ensemble-approach-to-large-scale-fuzzy-name-matching-b3e3fa124e3c
    # https://bergvca.github.io/2017/10/14/super-fast-string-matching.html
    # ING: https://medium.com/wbaa/https-medium-com-ingwbaa-boosting-selection-of-the-most-similar-entities-in-large-scale-datasets-450b3242e618
	vectorizer = TfidfVectorizer(min_df=1, analyzer=ngrams)
	if False:
		#SELF fit (find duplicates in df_psc)
		tf_idf_live = vectorizer.fit_transform(df_live["company_name"])	
		matches = awesome_cossim_top(tf_idf_live, tf_idf_live.transpose(), 10, 0.8)
		matches_df = get_matches_df(matches, df_live["company_name"],
									top=100000)
		matches_df = matches_df[matches_df['similarity'] < 0.99999] # Remove all exact matches
		matches_df.sample(20)


